import json
from base64 import b32encode
from binascii import unhexlify

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.http import HttpResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView, TemplateView, FormView
from django_otp.util import random_hex
from fido2 import cbor
from formtools.wizard.storage import get_storage
from mfa.FIDO2 import authenticate_complete
from two_factor.utils import default_device

from core import forms
from core.constants import AUTH_FORM_BY_TYPE, ADD_AUTH_FORM_BY_TYPE, AuthTypes
from core.mixins import AuthSessionDataMixin, AuthTypeMixin, AnonymousRequiredMixin


class RegistrationView(AnonymousRequiredMixin, CreateView):
    template_name = 'registration.html'
    model = get_user_model()
    form_class = forms.UserRegistrationForm
    success_url = reverse_lazy('auth:index')

    def form_valid(self, form):
        response = super().form_valid(form)
        if form.is_valid():
            messages.add_message(
                message='You were successfully registered. Now try to log in and add a new authentication method.',
                level=messages.SUCCESS, request=self.request
            )

        return response


class IndexView(AnonymousRequiredMixin, AuthTypeMixin, AuthSessionDataMixin, LoginView):
    template_name = 'index.html'
    success_url = reverse_lazy('auth:post_auth')

    def get(self, *args, **kwargs):
        if 'data_received' in self.request.session:
            del self.request.session['data_received']
        return super().get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'password_auth_form': AUTH_FORM_BY_TYPE[AuthTypes.PASSWORD],
            'digital_sign_auth_form': AUTH_FORM_BY_TYPE[AuthTypes.DIGITAL_SIGNATURE],
            'two_factor_auth_form': AUTH_FORM_BY_TYPE[AuthTypes.TWO_FACTOR],
            'biometric_auth_form': AUTH_FORM_BY_TYPE[AuthTypes.BIOMETRIC],
        })
        return context

    def form_valid(self, form):
        if self.auth_type == AuthTypes.TWO_FACTOR:
            self.request.session['base_username'] = form.user_cache.username
            return redirect(reverse_lazy('auth:tf_auth'))
        if self.auth_type == AuthTypes.BIOMETRIC:
            self.request.session['base_username'] = form.get_user().username
            return self.render_to_response(
                {**self.get_context_data(), form.form_name: None, 'active_field': self.auth_type}
            )
        return super().form_valid(form)

    def get_form(self, *args, **kwargs):
        return AUTH_FORM_BY_TYPE[self.auth_type](**self.get_form_kwargs())

    def form_invalid(self, form):
        if 'data_received' in self.request.session:
            del self.request.session['data_received']
        return self.render_to_response(
            {**self.get_context_data(), form.form_name: form, 'active_field': self.auth_type}
        )


class TwoFactorAuthView(AuthSessionDataMixin, LoginView):
    auth_type = AuthTypes.TWO_FACTOR
    template_name = 'two_factor_auth.html'
    form_class = forms.TwoFactorAuthForm
    success_url = reverse_lazy('auth:post_auth')

    def get_user(self):
        username = self.request.session['base_username']
        user = get_user_model().objects.filter(username=username).first()
        if not user:
            return redirect(reverse_lazy('auth:index'))
        return user

    def get(self, *args, **kwargs):
        if 'base_username' not in self.request.session:
            return redirect(reverse_lazy('auth:index'))
        default_device(self.get_user()).generate_challenge()
        return super().get(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'user': self.get_user(),
            'initial_device': None,
        })
        return kwargs


class PostAuthView(LoginRequiredMixin, TemplateView):
    template_name = 'post_auth.html'
    success_url = reverse_lazy('auth:index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        data_received = json.loads(self.request.session['data_received'])
        data_stored = {key: getattr(self.request.user, key, None) for key in data_received}
        table_data = {key: [data_received[key], data_stored[key]] for key in data_received}
        context.update({
            'table_data': table_data,
            'auth_type': self.request.session['auth_type'],
        })
        del self.request.session['data_received']
        del self.request.session['auth_type']
        return context

    def get(self, *args, **kwargs):
        if 'data_received' not in self.request.session or 'auth_type' not in self.request.session:
            return redirect(self.success_url)
        return super().get(*args, **kwargs)


class ProfileView(AuthTypeMixin, LoginRequiredMixin, FormView):
    default_auth_type = AuthTypes.TWO_FACTOR
    template_name = 'profile.html'
    auth_types = [AuthTypes.TWO_FACTOR, AuthTypes.DIGITAL_SIGNATURE]
    success_url = reverse_lazy('auth:index')

    def dispatch(self, request, *args, **kwargs):
        self.storage = get_storage(
            'formtools.wizard.storage.session.SessionStorage',
            self.prefix, request, getattr(self, 'file_storage', None),
        )
        return super(ProfileView, self).dispatch(request, *args, **kwargs)

    def get(self, *args, **kwargs):
        self.request.session['django_two_factor-qr_secret_key'] = b32encode(
            unhexlify(self.get_key().encode('ascii'))
        ).decode('utf-8')
        return super().get(*args, **kwargs)

    def get_key(self):
        if 'key' in self.storage.extra_data:
            return self.storage.extra_data['key']
        key = random_hex(20)
        self.storage.extra_data['key'] = key
        return key

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'digital_sign_form': ADD_AUTH_FORM_BY_TYPE[AuthTypes.DIGITAL_SIGNATURE],
            'two_factor_form': ADD_AUTH_FORM_BY_TYPE[AuthTypes.TWO_FACTOR](key=random_hex(20), user=self.request.user),
        })
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.auth_type == AuthTypes.TWO_FACTOR:
            kwargs.update({
                'key': self.get_key(),
                'user': self.request.user,
            })
        return kwargs

    def get_form(self, form_class=None):
        return ADD_AUTH_FORM_BY_TYPE[self.auth_type](**self.get_form_kwargs())

    def form_valid(self, form):
        messages.add_message(
            message=settings.AUTH_ADD_SUCCESS_MESSAGE_TEMPLATE.format(AuthTypes.pretty(self.auth_type)),
            level=messages.SUCCESS, request=self.request
        )

        if self.auth_type == AuthTypes.DIGITAL_SIGNATURE:
            signature = self.request.user.add_signature(self.request.POST['key_password'])
            response = HttpResponse(content_type='text/plain')
            filename = f"{self.request.user.username}_signature_{timezone.now()}.txt"
            response['Content-Disposition'] = f'attachment; filename={filename}'
            response.write(signature)
            return response

        if self.auth_type == AuthTypes.TWO_FACTOR:
            form.save()
            logout(self.request)
            return redirect(self.get_success_url())

        return super().form_valid(form)

    def form_invalid(self, form):
        return self.render_to_response(
            {**self.get_context_data(), form.form_name: form, 'active_field': self.auth_type}
        )


@method_decorator(csrf_exempt, name='dispatch')
class BiometricAuthAjaxView(AuthSessionDataMixin, View):
    success_url = reverse_lazy('auth:post_auth')
    auth_type = AuthTypes.BIOMETRIC

    def post(self, request, *args, **kwargs):
        self.additional_data = cbor.decode(request.body)
        super().post(request, *args, **kwargs)
        return authenticate_complete(request)
