from django.contrib.auth import login, get_user_model
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.serializers.json import DjangoJSONEncoder
from django.urls import reverse


class CustomJsonEncoder(DjangoJSONEncoder):
    def default(self, o):
        if isinstance(o, InMemoryUploadedFile):
            o.seek(0)
            return o.read()
        return str(o)


def custom_login(request, username):
    user = get_user_model().objects.get(username=username)
    login(request, user=user)
    return {'location': reverse('auth:post_auth')}
