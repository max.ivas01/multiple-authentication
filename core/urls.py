from django.contrib.auth.views import LogoutView
from django.urls import path, include
from mfa import FIDO2
from two_factor.views import QRGeneratorView

from core import views

app_name = 'auth'

biometry_urls = [
    path('begin_auth/', FIDO2.authenticate_begin, name="biometric_auth_begin"),
    path('complete_auth/', views.BiometricAuthAjaxView.as_view(), name='biometric_auth_complete'),
    path('begin_reg/', FIDO2.begin_registeration, name="biometric_reg_begin"),
    path('complete_reg/', FIDO2.complete_reg, name="biometric_reg_complete"),
]

two_factor_urls = [
    path('qr', QRGeneratorView.as_view(), name='tf_qr'),
    path('', views.TwoFactorAuthView.as_view(), name='tf_auth'),
]


urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('registration/', views.RegistrationView.as_view(), name='registration'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('postauth/', views.PostAuthView.as_view(), name='post_auth'),
    path('profile/<pk>', views.ProfileView.as_view(), name='profile'),

    path('two_factor/', include(two_factor_urls)),
    path('biometric/', include(biometry_urls)),
]
