from unittest.mock import patch, MagicMock

from django.contrib.auth import get_user_model, get_user
from django.contrib.messages import get_messages
from django.test import TestCase
from django.urls import reverse_lazy
from mfa.models import User_Keys as UserKeys

from core.constants import AuthTypes


class RegistrationViewTestCase(TestCase):
    url = reverse_lazy('auth:registration')

    @classmethod
    def setUpTestData(cls):
        cls.user = get_user_model().objects.create()

    def test_get_logged_user(self):
        self.client.force_login(self.user)

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(get_user(self.client).is_authenticated)

    def test_get_anonymous_user(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(get_user(self.client).is_authenticated)

    def test_post_success(self):
        data = {
            'username': 'Username',
            'password1': 'SomeHardPassword',
            'password2': 'SomeHardPassword',
        }
        expected_url = reverse_lazy('auth:index')

        response = self.client.post(self.url, data=data)

        self.assertRedirects(response, expected_url, status_code=302, target_status_code=200)
        self.assertFalse(get_user(self.client).is_authenticated)
        self.assertTrue(get_user_model().objects.filter(username=data['username']).exists())
        self.assertEqual(len(get_messages(response.wsgi_request)), 1)

    def test_post_errors(self):
        data = {
            'username': 'Username',
            'password1': 'SomeHardPassword',
        }

        response = self.client.post(self.url, data=data)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(get_user(self.client).is_authenticated)
        self.assertFalse(get_user_model().objects.filter(username=data['username']).exists())
        self.assertFormError(response, 'form', 'password2', 'This field is required.')
        self.assertEqual(len(get_messages(response.wsgi_request)), 0)


class IndexViewTestCase(TestCase):
    url = reverse_lazy('auth:index')
    success_url = reverse_lazy('auth:post_auth')

    @classmethod
    def setUpTestData(cls):
        cls.user_password = 'SomeHardPassword'
        cls.user = get_user_model().objects.create_user(username='Username', password=cls.user_password)

    def test_get_logged_user(self):
        self.client.force_login(self.user)

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(get_user(self.client).is_authenticated)

    def test_get_anonymous_user(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(get_user(self.client).is_authenticated)

    def test_get_with_session_data(self):
        self.client.session['data_received'] = 'Some data'
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(get_user(self.client).is_authenticated)
        self.assertNotIn('data_received', self.client.session)

    # Password auth tests

    def test_post_password_auth_success(self):
        data = {
            'username': self.user.username,
            'password': self.user_password,
            'auth_type': AuthTypes.PASSWORD,
        }

        response = self.client.post(self.url, data=data)

        self.assertIn('data_received', self.client.session)
        self.assertRedirects(response, self.success_url, status_code=302, target_status_code=200)
        self.assertTrue(get_user(self.client).is_authenticated)
        self.assertEqual(get_user(self.client), self.user)

    def test_post_password_auth_errors(self):
        data = {
            'username': self.user.username,
            'auth_type': AuthTypes.PASSWORD,
        }

        response = self.client.post(self.url, data=data)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(get_user(self.client).is_authenticated)
        self.assertFormError(response, 'password_auth_form', 'password', 'This field is required.')
        self.assertEqual(len(get_messages(response.wsgi_request)), 0)

    # Two factor auth tests (first step)

    @patch('core.views.default_device')
    def test_post_two_factor_auth_success(self, tfa_mock):
        def_device_mock = tfa_mock.return_value.generate_challenge
        tfa_mock.return_value = def_device_mock

        two_factor_second_step_url = reverse_lazy('auth:tf_auth')
        data = {
            'username': self.user.username,
            'password': self.user_password,
            'auth_type': AuthTypes.TWO_FACTOR,
        }

        response = self.client.post(self.url, data=data)

        self.assertIn('data_received', self.client.session)
        self.assertIn('base_username', self.client.session)
        self.assertEqual(self.client.session['base_username'], self.user.username)
        self.assertRedirects(response, two_factor_second_step_url, status_code=302, target_status_code=200)
        self.assertFalse(get_user(self.client).is_authenticated)

    def test_post_two_factor_auth_errors(self):
        data = {
            'username': self.user.username,
            'auth_type': AuthTypes.TWO_FACTOR,
        }

        response = self.client.post(self.url, data=data)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(get_user(self.client).is_authenticated)
        self.assertFormError(response, 'two_factor_auth_form', 'password', 'This field is required.')
        self.assertEqual(len(get_messages(response.wsgi_request)), 0)

    # Digital Signature auth tests

    def test_post_digital_signature_auth_success(self):
        file_mock = MagicMock()
        file_mock.read.return_value = self.user.add_signature('key_password')

        data = {
            'digital_signature': file_mock,
            'key_password': 'key_password',
            'auth_type': AuthTypes.DIGITAL_SIGNATURE,
        }

        response = self.client.post(self.url, data=data)

        self.assertIn('data_received', self.client.session)
        self.assertRedirects(response, self.success_url, status_code=302, target_status_code=200)
        self.assertTrue(get_user(self.client).is_authenticated)
        self.assertEqual(get_user(self.client), self.user)

    def test_post_digital_signature_auth_errors(self):
        file_mock = MagicMock()
        file_mock.read.return_value = self.user.add_signature('key_password')

        data = {
            'digital_signature': file_mock,
            'key_password': 'wrong_key',
            'auth_type': AuthTypes.DIGITAL_SIGNATURE,
        }

        response = self.client.post(self.url, data=data)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(get_user(self.client).is_authenticated)
        self.assertFormError(response, 'digital_sign_auth_form', 'key_password', 'Signature verification failed.')

    # Biometric auth tests (first step)

    def test_post_biometric_auth_success(self):
        UserKeys.objects.create(username=self.user.username)

        data = {
            'username': self.user.username,
            'auth_type': AuthTypes.BIOMETRIC,
        }

        response = self.client.post(self.url, data=data)

        self.assertIn('data_received', self.client.session)
        self.assertIn('base_username', self.client.session)
        self.assertEqual(self.client.session['base_username'], self.user.username)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(get_user(self.client).is_authenticated)

    def test_post_biometric_auth_errors(self):
        file_mock = MagicMock()
        file_mock.read.return_value = self.user.add_signature('key_password')

        data = {
            'username': 'WrongUsername',
            'auth_type': AuthTypes.BIOMETRIC,
        }

        response = self.client.post(self.url, data=data)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(get_user(self.client).is_authenticated)
        self.assertFormError(response, 'biometric_auth_form', 'username', 'Username is incorrect')


class TwoFactorAuthViewTestCase(TestCase):
    url = reverse_lazy('auth:tf_auth')

    @classmethod
    def setUpTestData(cls):
        cls.user = get_user_model().objects.create_user(username='Username', password='Password')

    @patch('core.views.default_device')
    def test_get_success(self, tfa_mock):
        def_device_mock = tfa_mock.return_value.generate_challenge
        tfa_mock.return_value = def_device_mock

        session = self.client.session   # every self.client.session returns new object, so it must be put in variable
        session['base_username'] = self.user.username
        session.save()

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(get_user(self.client).is_authenticated)

    def test_get_no_base_username(self):
        expected_url = reverse_lazy('auth:index')

        response = self.client.get(self.url)

        self.assertRedirects(response, expected_url, status_code=302, target_status_code=200)
        self.assertFalse(get_user(self.client).is_authenticated)

    @patch('django_otp.forms.OTPAuthenticationFormMixin._chosen_device')
    @patch('core.views.default_device')
    def test_post_success(self, tfa_mock, device_mock):
        session = self.client.session  # every self.client.session returns new object, so it must be put in variable
        session['base_username'] = self.user.username
        session.save()

        def_device_mock = tfa_mock.return_value.generate_challenge
        tfa_mock.return_value = def_device_mock
        device_mock.return_value.verify_is_allowed.return_value = (True, None)
        device_mock.return_value.verify_token.return_value = True

        expected_url = reverse_lazy('auth:post_auth')
        data = {
            'otp_token': '12345'
        }

        response = self.client.post(self.url, data=data)

        self.assertRedirects(response, expected_url, status_code=302, target_status_code=200)
        self.assertTrue(get_user(self.client).is_authenticated)

    @patch('django_otp.forms.OTPAuthenticationFormMixin._chosen_device')
    @patch('core.views.default_device')
    def test_post_errors(self, tfa_mock, device_mock):
        session = self.client.session  # every self.client.session returns new object, so it must be put in variable
        session['base_username'] = self.user.username
        session.save()

        def_device_mock = tfa_mock.return_value.generate_challenge
        tfa_mock.return_value = def_device_mock
        device_mock.return_value.verify_is_allowed.return_value = (True, None)
        device_mock.return_value.verify_token.return_value = False

        data = {
            'otp_token': '12345'
        }

        response = self.client.post(self.url, data=data)

        self.assertTrue(response.status_code, 200)

        self.assertFalse(get_user(self.client).is_authenticated)
        self.assertFormError(response, 'form', None, 'Invalid token. Please make sure you have entered it correctly.')
