from django.utils.regex_helper import Choice

from core import forms


class AuthTypes(Choice):
    PASSWORD = 'password'
    TWO_FACTOR = 'two_factor'
    DIGITAL_SIGNATURE = 'digital_sign'
    BIOMETRIC = 'biometric'

    @classmethod
    def list(cls):
        return [cls.PASSWORD, cls.TWO_FACTOR, cls.DIGITAL_SIGNATURE, cls.BIOMETRIC]

    @classmethod
    def pretty(cls, auth_type=None):
        pretty = {
            cls.PASSWORD: 'Password',
            cls.TWO_FACTOR: 'Two Factor',
            cls.DIGITAL_SIGNATURE: 'Digital Signature',
            cls.BIOMETRIC: 'Biometric'
        }
        if not auth_type:
            return pretty
        if auth_type not in cls.list():
            raise ValueError(f'{auth_type} is not presented in AuthTypes')
        return pretty[auth_type]


AUTH_FORM_BY_TYPE = {
    AuthTypes.PASSWORD: forms.PasswordAuthForm,
    AuthTypes.DIGITAL_SIGNATURE:  forms.DigitalSignatureAuthForm,
    AuthTypes.TWO_FACTOR: forms.TwoFactorPreAuthForm,   # First step
    AuthTypes.BIOMETRIC: forms.BiometricAuthForm,    # First step
}

ADD_AUTH_FORM_BY_TYPE = {
    AuthTypes.DIGITAL_SIGNATURE:  forms.DigitalSignatureForm,
    AuthTypes.TWO_FACTOR: forms.TwoFactorForm,
}
