import json

from django.contrib.auth import logout
from django.shortcuts import redirect

from core.constants import AuthTypes
from core.utils import CustomJsonEncoder


class AnonymousRequiredMixin:
    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            logout(self.request)
        return super().get(*args, **kwargs)


class AuthSessionDataMixin:
    auth_type = AuthTypes.PASSWORD
    EXCLUDE_FIELDS = ['csrfmiddlewaretoken', 'auth_type']
    success_url = None
    additional_data = {}

    def post(self, request, *args, **kwargs):
        response = getattr(super(), 'post', None)
        data_received = {key: value[0] if isinstance(value, list) else value for key, value in
                         {**request.FILES, **request.POST, **self.additional_data}.items()
                         if key not in self.EXCLUDE_FIELDS}
        if 'data_received' in self.request.session:
            data = json.loads(self.request.session['data_received'])
            data.update(**data_received)
            self.request.session.update({
                'data_received': json.dumps(data, cls=CustomJsonEncoder),
                'auth_type': AuthTypes.pretty(self.auth_type),
            })
        else:
            self.request.session.update({
                'data_received': json.dumps(data_received, cls=CustomJsonEncoder),
                'auth_type': AuthTypes.pretty(self.auth_type),
            })
        return response(request, *args, **kwargs) if response else redirect(
            getattr(self, 'get_success_url', None) or self.success_url)


class AuthTypeMixin:
    default_auth_type = AuthTypes.PASSWORD
    auth_types = []

    def dispatch(self, request, *args, **kwargs):
        self.auth_type = request.POST.get('auth_type', self.default_auth_type)
        allowed_auth_types = self.auth_types or AuthTypes.list()
        if self.auth_type not in allowed_auth_types:
            return redirect(request.path_info)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['active_field'] = self.auth_type
        return context
