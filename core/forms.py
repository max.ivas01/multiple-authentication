from hashlib import md5

from django import forms
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import get_user_model
from ecdsa import SigningKey, BadSignatureError
from mfa.models import User_Keys as UserKeys
from two_factor.forms import AuthenticationTokenForm, TOTPDeviceForm


class UserRegistrationForm(UserCreationForm):

    class Meta:
        model = get_user_model()
        fields = ['username', 'password1', 'password2']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in ['username', 'password1', 'password2']:
            self.fields[field].help_text = None


class DigitalSignatureForm(forms.Form):
    form_name = 'digital_sign_form'

    key_password = forms.CharField(min_length=15)


class PasswordAuthForm(AuthenticationForm):
    form_name = 'password_auth_form'


class DigitalSignatureAuthForm(AuthenticationForm):
    form_name = 'digital_sign_auth_form'

    digital_signature = forms.FileField()
    key_password = forms.CharField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in ['username', 'password']:
            del self.fields[field]

    def get_user(self):
        digital_signature = self.cleaned_data['digital_signature']
        digital_signature.seek(0)
        signature = digital_signature.read()
        return get_user_model().objects.filter(digital_signature=signature).first()

    def clean_digital_signature(self):
        digital_signature = self.cleaned_data['digital_signature']
        user = self.get_user()
        if not user:
            raise forms.ValidationError('This digital signature is not registered in system.')
        return digital_signature

    def clean(self):
        digital_signature = self.cleaned_data.get('digital_signature')
        key_password = self.cleaned_data.get('key_password')

        if not digital_signature or not key_password:
            return super().clean()

        user = self.get_user()

        digital_signature.seek(0)
        signature = digital_signature.read()

        private_key = SigningKey.from_string(str.encode(md5(str.encode(key_password)).hexdigest()[:24]))
        public_key = private_key.verifying_key

        try:
            public_key.verify(signature, str.encode(
                settings.DIGITAL_SIGNATURE_PLAIN_TEXT_TEMPLATE.format(
                    user.username, user.password, user.date_joined
                )
            ))
        except BadSignatureError:
            self.add_error('key_password', 'Signature verification failed.')


class TwoFactorPreAuthForm(AuthenticationForm):
    form_name = 'two_factor_auth_form'


class TwoFactorAuthForm(AuthenticationTokenForm):
    form_name = 'two_factor_auth_form'

    def __init__(self, *args, **kwargs):
        kwargs.pop('request', None)
        super().__init__(*args, **kwargs)

    def get_user(self):
        return self.user


class TwoFactorForm(TOTPDeviceForm):
    form_name = 'two_factor_form'


class BiometricAuthForm(AuthenticationForm):
    form_name = 'biometric_auth_form'

    username = forms.CharField(max_length=100)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        del self.fields['password']

    def get_user(self):
        username = self.cleaned_data['username']
        return get_user_model().objects.get(username=username)

    def clean_username(self):
        username = self.cleaned_data['username']
        if not get_user_model().objects.filter(username=username).exists():
            raise forms.ValidationError('Username is incorrect')
        if not UserKeys.objects.filter(username=username).exists():
            raise forms.ValidationError("You haven't set up biometric authentication yet")
        return username
