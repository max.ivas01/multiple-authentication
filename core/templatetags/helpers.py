from django import template
from django.urls import reverse

register = template.Library()


@register.simple_tag(takes_context=True)
def absolute_url(context, view_name, *args, **kwargs):
    request = context['request']
    return f'https://{request.get_host()}{reverse(view_name, args=args, kwargs=kwargs)}'
