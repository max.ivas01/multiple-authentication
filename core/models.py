from hashlib import md5

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from ecdsa import SigningKey


class AuthUser(AbstractUser):
    digital_signature = models.CharField(max_length=500)

    def add_signature(self, key_password):
        private_key = SigningKey.from_string(str.encode(md5(str.encode(key_password)).hexdigest()[:24]))
        signature = private_key.sign(
            str.encode(settings.DIGITAL_SIGNATURE_PLAIN_TEXT_TEMPLATE.format(
                self.username, self.password, self.date_joined
            )))

        self.digital_signature = signature
        self.save()
        return signature
