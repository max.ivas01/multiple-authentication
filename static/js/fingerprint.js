
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// THIS JS IS UGLY. PLEASE, LET ID DIE PEACEFULLY
// TODO: Refactor
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


function redirect_to_profile() {
    window.location.href = $('#success_url').data('url-selected');

}

function begin_reg() {
    const begin_reg_url = $('#biometry_begin_reg').data('url-selected'),
        complete_reg_url = $('#biometry_complete_reg').data('url-selected');

    fetch(begin_reg_url, {}
    ).then(function (response) {
        if (response.ok) {
            return response.arrayBuffer();
        }
        throw new Error('Error getting registration data!');
    }).then(CBOR.decode).then(function (options) {
        options.publicKey.attestation = "direct"
        return navigator.credentials.create(options);
    }).then(function (attestation) {
        return fetch(complete_reg_url, {
            method: 'POST',
            headers: {'Content-Type': 'application/cbor'},
            body: CBOR.encode({
                "attestationObject": new Uint8Array(attestation.response.attestationObject),
                "clientDataJSON": new Uint8Array(attestation.response.clientDataJSON),
            })
        });
    }).then(function (response) {
        return response.json()
    }).then(function (res) {
        if (res["status"] === 'OK') {
            redirect_to_profile()
        } else {
            $("#res").html("<div class='alert alert-danger'>Registration Failed. Please, reload the page and try again</div>")
        }


    }, function (reason) {
        $("#res").html("<div class='alert alert-danger'>Registration Failed. Please, reload the page and try again</div>")
    })
}


function authen() {
    const begin_auth_url = $('#biometry_begin_auth').data('url-selected'),
        complete_auth_url = $('#biometry_complete_auth').data('url-selected');
    fetch(begin_auth_url, {
        method: 'GET',
    }).then(function (response) {
        if (response.ok) return response.arrayBuffer();
        throw new Error('No credential available to authenticate!');
    }).then(CBOR.decode).then(function (options) {
        console.log(options)
        return navigator.credentials.get(options);
    }).then(function (assertion) {
        res = CBOR.encode({
            "credentialId": new Uint8Array(assertion.rawId),
            "authenticatorData": new Uint8Array(assertion.response.authenticatorData),
            "clientDataJSON": new Uint8Array(assertion.response.clientDataJSON),
            "signature": new Uint8Array(assertion.response.signature)
        });

        return fetch(complete_auth_url, {

            method: 'POST',
            headers: {'Content-Type': 'application/cbor'},
            body: res,

        }).then(function (response) {
            if (response.ok) return res = response.json()
        }).then(function (res) {

            if (res.status == "OK") {
                $("#msgdiv").addClass("alert alert-success").removeClass("alert-danger")
                $("#msgdiv").html("Verified....please wait")
                window.location.href = res.redirect;
            } else {
                $("#msgdiv").addClass("alert alert-danger").removeClass("alert-success")
                $("#msgdiv").html("Verification Failed as " + res.message + ", <a href='javascript:void(0)' onclick='authen())'> try again</a> or <a href='javascript:void(0)' onclick='history.back()'> Go Back</a>")


                mfa_failed_function();
            }
        })

    })

}

$(document).ready(function () {
    $('#add-biometry').on('click', function () {
        ua = new UAParser().getResult()
        if (ua.browser.name == "Safari" || ua.browser.name == "Mobile Safari") {
            $("#res").html("<button class='btn btn-success' onclick='begin_reg()'>Start...</button>")
        } else {
            setTimeout(begin_reg, 500)
        }
    })
    $('#auth-biometry').on('click', function () {
        if (location.protocol != 'https:') {
            $("#main_paragraph").addClass("alert alert-danger")
            $("#main_paragraph").html("FIDO2 must work under secure context")
        } else {
            ua = new UAParser().getResult()
            if (ua.browser.name == "Safari" || ua.browser.name == "Mobile Safari")
                $("#res").html("<button class='btn btn-success' onclick='authen()'>Authenticate...</button>")
            else
                authen()
        }
    })
})
